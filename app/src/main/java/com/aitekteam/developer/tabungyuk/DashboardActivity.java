package com.aitekteam.developer.tabungyuk;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.aitekteam.developer.tabungyuk.adapters.DashboardViewpagerAdapter;
import com.google.android.material.tabs.TabLayout;

public class DashboardActivity extends AppCompatActivity {

    // Declare Variable
    private ActionBar actionBar;
    private TabLayout tabLayout;

    /**
     * OnCreate Activity
     * @param savedInstanceState = Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        // Initialization variable
        Toolbar toolbar = findViewById(R.id.top_bar);
        this.tabLayout = findViewById(R.id.tabs);
        final ViewPager viewPager = findViewById(R.id.pager);

        // Set Action Bar
        setSupportActionBar(toolbar);
        this.actionBar = getSupportActionBar();

        // Set Viewpager
        viewPager.setAdapter(new DashboardViewpagerAdapter(getSupportFragmentManager(), this));

        // Set Tab Layout
        this.tabLayout.setupWithViewPager(viewPager);
        this.tabLayout.setOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                // Set Title Fragment
                setTitleFragment(tabLayout.getSelectedTabPosition());
                viewPager.setCurrentItem(tabLayout.getSelectedTabPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        // Set Title Fragment
        this.setTitleFragment(this.tabLayout.getSelectedTabPosition());
    }

    /**
     * Set Title Fragment
     * @param position = integer
     */
    private void setTitleFragment(int position) {
        switch (position) {
            case 0:
                this.actionBar.setTitle(getString(R.string.dasboard_name));
                break;
            case 1:
                this.actionBar.setTitle(getString(R.string.inclusion_name));
                break;
            default:
                this.actionBar.setTitle(getString(R.string.expense_name));
                break;
        }
    }

    /**
     * Set Up Option Menu
     * @param menu = Menu
     * @return boolean
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.item_shop) {
            Intent intent = new Intent(this, MarketActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
