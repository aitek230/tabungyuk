package com.aitekteam.developer.tabungyuk;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aitekteam.developer.tabungyuk.adapters.GardenAdapter;
import com.aitekteam.developer.tabungyuk.helpers.DatabaseViewModel;
import com.aitekteam.developer.tabungyuk.interfaces.CustomOnClickListener;
import com.aitekteam.developer.tabungyuk.models.entities.Garden;
import com.aitekteam.developer.tabungyuk.models.entities.Users;

import java.util.List;

public class GardenActivity extends AppCompatActivity {

    // Declare Variable
    private DatabaseViewModel db;
    private Users profile;
    private GardenAdapter adapter;
    private RelativeLayout emptyView;
    private Observer<Users> liveProfile = new Observer<Users>() {
        @Override
        public void onChanged(Users users) {
            if (users != null) {
                profile = users;
            }
        }
    };
    private Observer<List<Garden>> liveGarden = new Observer<List<Garden>>() {
        @Override
        public void onChanged(List<Garden> garden) {
            emptyView.setVisibility(View.GONE);
            if (garden != null) {
                if (garden.size() > 0) {
                    adapter.setData(garden);
                }
                else emptyView.setVisibility(View.VISIBLE);
            }
            else emptyView.setVisibility(View.VISIBLE);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_garden);

        // Initialization variable
        Toolbar toolbar = findViewById(R.id.top_bar);

        // Initialization Lists
        RecyclerView lists = findViewById(R.id.lists);

        // Initialization Empty View
        this.emptyView = findViewById(R.id.empty_view);

        // Initialization Database View Model
        this.db = new ViewModelProvider(this).get(DatabaseViewModel.class);

        // Set Action Bar
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();

        // Set Linear Layout Manager To Recycler view
        lists.setLayoutManager(new LinearLayoutManager(this));

        actionBar.setTitle(R.string.garden_name);

        this.adapter = new GardenAdapter(new CustomOnClickListener() {
            @Override
            public void onItemClick(Object item, int position, int action) {

            }
        });

        lists.setAdapter(adapter);

        // Set Up Live Profile
        LiveData<Users> liveDataProfile = this.db.liveProfile();
        liveDataProfile.observe(this, liveProfile);

        // Set Up Live Gardens
        LiveData<List<Garden>> liveDataGarden = this.db.getGardens();
        liveDataGarden.observe(this, liveGarden);
    }
}
