package com.aitekteam.developer.tabungyuk;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.aitekteam.developer.tabungyuk.helpers.DatabaseViewModel;
import com.aitekteam.developer.tabungyuk.helpers.RandomString;
import com.aitekteam.developer.tabungyuk.models.entities.Users;

public class InitialActivity extends AppCompatActivity {

    private DatabaseViewModel db;
    private EditText fullName;
    private Button save;
    private Observer<Users> liveProfile = new Observer<Users>() {
        @Override
        public void onChanged(Users users) {
            if (users != null) {
                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial);

        // Initialization Database View Model
        this.db = new ViewModelProvider(this).get(DatabaseViewModel.class);
        this.fullName = findViewById(R.id.initial_name);
        this.save = findViewById(R.id.initial_save);

        if (db.checkAlreadyInstaller()) {
            Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(intent);
            finish();
        }

        // Set Up Live Profile
        LiveData<Users> liveDataProfile = this.db.liveProfile();
        liveDataProfile.observe(this, liveProfile);

        this.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(fullName.getText().toString())) {
                    Toast.makeText(getApplicationContext(), new StringBuilder().append("Harap isi nama lengkap"), Toast.LENGTH_LONG).show();
                }
                else {
                    // Check If Already Have Account
                    Users newUser = new Users();
                    newUser.setUser_id("");
                    newUser.setUsername(RandomString.getAlphaNumericString(20));
                    newUser.setPassword("");
                    newUser.setFull_name(fullName.getText().toString());
                    newUser.setFace("");
                    newUser.setWallet(0);
                    newUser.setWallet_in(0);
                    newUser.setWallet_out(0);
                    newUser.setPoint(0);
                    newUser.setCreated_at(System.currentTimeMillis());
                    newUser.setUpdated_at(System.currentTimeMillis());
                    db.doAction(newUser, DatabaseViewModel.ACTION_INSERT, DatabaseViewModel.TB_USER);
                }
            }
        });
    }
}
