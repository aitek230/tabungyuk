package com.aitekteam.developer.tabungyuk;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aitekteam.developer.tabungyuk.adapters.MarketAdapter;
import com.aitekteam.developer.tabungyuk.helpers.DatabaseViewModel;
import com.aitekteam.developer.tabungyuk.helpers.MarketDialog;
import com.aitekteam.developer.tabungyuk.interfaces.CustomOnClickListener;
import com.aitekteam.developer.tabungyuk.models.data.ItemMarket;
import com.aitekteam.developer.tabungyuk.models.entities.Garden;
import com.aitekteam.developer.tabungyuk.models.entities.Users;

import java.util.ArrayList;
import java.util.List;

public class MarketActivity extends AppCompatActivity implements MarketDialog.MarketDialogListener {

    // Declare Variable
    private DatabaseViewModel db;
    private ActionBar actionBar;
    private TextView point;
    private Users profile;
    private RecyclerView lists;
    private Observer<Users> liveProfile = new Observer<Users>() {
        @Override
        public void onChanged(Users users) {
            if (users != null) {
                profile = users;
                point.setText(new StringBuilder().append(users.getPoint()));
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market);

        // Initialization variable
        Toolbar toolbar = findViewById(R.id.top_bar);
        this.point = findViewById(R.id.point);

        // Initialization Lists
        lists = findViewById(R.id.lists);

        // Initialization Database View Model
        this.db = new ViewModelProvider(this).get(DatabaseViewModel.class);

        // Set Action Bar
        setSupportActionBar(toolbar);
        this.actionBar = getSupportActionBar();

        // Set Linear Layout Manager To Recycler view
        lists.setLayoutManager(new LinearLayoutManager(this));

        // Set Up
        this.setUp();
    }

    private void setUp() {
        this.actionBar.setTitle(R.string.market_name);

        List<ItemMarket> data = new ArrayList<>();
        data.add(new ItemMarket("Anggrek", "Bunga Anggrek nggak hanya cantik, tapi juga memiliki nilai estetika ketika dijadikan sebagai tanaman hias untuk suatu ruangan.",
                R.mipmap.orchid, 100));
        data.add(new ItemMarket("Melati", "Bunga Melati dinobatkan sebagai bunga anggun, bunga ini juga memilik aroma yang khas sehingga menjadi simbol dari kesucian.",
                R.mipmap.jasmine, 100));
        data.add(new ItemMarket("Mawar", "Bunga Mawar walaupun berduri tapi sangat mengikat, memiliki daya tarik tersendiri bagaikan seorang pasangan yang mempunyai ciri khas.",
                R.mipmap.rose, 100));
        data.add(new ItemMarket("Lidah Mertua", "Lidah Mertua tumbuh secara vertikal yang menandakan rejeki yang semakin hari semakin bertambah, cocok untuk pajangan di teras rumah.",
                R.mipmap.tongue, 200));
        data.add(new ItemMarket("Cabe Pelangi", "Cabe Pelangi kecil, pedas, tapi memikat itulah istilah yang cocok untuk yang satu ini, dimana termasuk dalam tanaman langkah karena perlu tatacara perawatan khusus.",
                R.mipmap.chili, 320));
        data.add(new ItemMarket("Bunga Banci", "Bunga banci tidak seperti namanya yang banci, bunga ini adalah simbol dari kreativitas pemiliknya, jenis langkah, dan harga nya yang ekslusif.",
                R.mipmap.pansy, 500));

        // Set Up Live Profile
        LiveData<Users> liveDataProfile = this.db.liveProfile();
        liveDataProfile.observe(this, liveProfile);

        MarketAdapter adapter = new MarketAdapter(data, new CustomOnClickListener() {
            @Override
            public void onItemClick(Object item, int position, int action) {
                ItemMarket itemMarket = (ItemMarket) item;

                // Initialization Complete Dialog
                DialogFragment dialog = new MarketDialog(MarketActivity.this,
                        R.layout.dialog_detail_market, itemMarket.getName(), itemMarket);

                // Show Dialog
                dialog.show(MarketActivity.this.getSupportFragmentManager(), "Market Detail");
            }
        });
        this.lists.setAdapter(adapter);
    }

    /**
     * Set Up Option Menu
     * @param menu = Menu
     * @return boolean
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.market_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.item_profile:
                intent = new Intent(getApplicationContext(), ProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.item_garden:
                intent = new Intent(getApplicationContext(), GardenActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onShow(final DialogFragment dialog, View view, final ItemMarket data) {
        ImageView icon;
        TextView price, description;
        Button buy;

        icon = view.findViewById(R.id.icon);
        price = view.findViewById(R.id.price);
        description = view.findViewById(R.id.description);
        buy = view.findViewById(R.id.buy);

        icon.setImageResource(data.getIcon());
        price.setText(new StringBuilder().append("Harga ").append(data.getPrice()));
        description.setText(data.getDescription());
        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (profile.getPoint() < data.getPrice()) {
                    Toast.makeText(MarketActivity.this, R.string.msg_failed_to_buy, Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                    return;
                }

                Garden newGarden = new Garden();
                newGarden.setCreated_at(System.currentTimeMillis());
                newGarden.setDescription(data.getDescription());
                newGarden.setIcon(data.getIcon());
                newGarden.setName(data.getName());
                newGarden.setPrice(data.getPrice());
                newGarden.setUpdated_at(System.currentTimeMillis());
                db.doAction(newGarden, DatabaseViewModel.ACTION_INSERT, DatabaseViewModel.TB_GARDEN);

                Toast.makeText(MarketActivity.this, R.string.msg_success_to_buy, Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });
    }
}
