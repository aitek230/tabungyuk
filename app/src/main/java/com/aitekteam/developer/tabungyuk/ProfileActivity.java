package com.aitekteam.developer.tabungyuk;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.aitekteam.developer.tabungyuk.helpers.DatabaseViewModel;
import com.aitekteam.developer.tabungyuk.models.entities.Users;

public class ProfileActivity extends AppCompatActivity {

    // Declare Variable
    private DatabaseViewModel db;
    private ActionBar actionBar;
    private EditText fullName, userName, password;
    private Button save;
    private Users profile;
    private Observer<Users> liveProfile = new Observer<Users>() {
        @Override
        public void onChanged(Users users) {
            if (users != null) {
                profile = users;
                fullName.setText(profile.getFull_name());
                userName.setText(profile.getUsername());
                if (TextUtils.isEmpty(profile.getPassword()))
                    password.setText(new StringBuilder().append("Belum di set"));
                else password.setText(profile.getPassword());
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        // Initialization variable
        Toolbar toolbar = findViewById(R.id.top_bar);

        // Initialization Database View Model
        this.db = new ViewModelProvider(this).get(DatabaseViewModel.class);

        // Set Action Bar
        setSupportActionBar(toolbar);

        this.actionBar = getSupportActionBar();
        this.fullName = findViewById(R.id.profile_name);
        this.userName = findViewById(R.id.profile_username);
        this.password = findViewById(R.id.profile_password);
        this.save = findViewById(R.id.profile_save);

        this.actionBar.setTitle(R.string.profile_name);

        // Set Up Live Profile
        LiveData<Users> liveDataProfile = this.db.liveProfile();
        liveDataProfile.observe(this, liveProfile);

        this.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (profile != null) {
                    profile.setFull_name(fullName.getText().toString());
                    profile.setUsername(userName.getText().toString());
                    profile.setPassword(password.getText().toString());

                    db.doAction(profile, DatabaseViewModel.ACTION_EDIT, DatabaseViewModel.TB_USER);
                    Toast.makeText(getApplicationContext(), R.string.msg_success_update_profile, Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
