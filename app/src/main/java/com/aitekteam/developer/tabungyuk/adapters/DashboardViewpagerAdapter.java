package com.aitekteam.developer.tabungyuk.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.aitekteam.developer.tabungyuk.R;
import com.aitekteam.developer.tabungyuk.fragments.ExpenseFragment;
import com.aitekteam.developer.tabungyuk.fragments.HomeFragment;
import com.aitekteam.developer.tabungyuk.fragments.InclusionFragment;

public class DashboardViewpagerAdapter extends FragmentStatePagerAdapter {

    private Context context;
    public DashboardViewpagerAdapter(@NonNull FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: return new HomeFragment();
            case 1: return new InclusionFragment();
            default: return new ExpenseFragment();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0: return this.context.getString(R.string.dasboard_name);
            case 1: return this.context.getString(R.string.inclusion_name);
            default: return this.context.getString(R.string.expense_name);
        }
    }
}
