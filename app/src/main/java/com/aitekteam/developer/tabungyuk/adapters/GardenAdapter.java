package com.aitekteam.developer.tabungyuk.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aitekteam.developer.tabungyuk.R;
import com.aitekteam.developer.tabungyuk.interfaces.CustomOnClickListener;
import com.aitekteam.developer.tabungyuk.models.entities.Garden;

import java.util.ArrayList;
import java.util.List;

public class GardenAdapter extends RecyclerView.Adapter<GardenAdapter.ViewHolder> {

    private List<Garden> data;
    private CustomOnClickListener handler;

    public GardenAdapter(CustomOnClickListener handler) {
        this.data = new ArrayList<>();
        this.handler = handler;
    }

    public void setData(List<Garden> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_market, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Garden item = this.data.get(position);
        holder.point.setText(new StringBuilder().append(item.getPrice()));
        if (item.getDescription().length() > 32) holder.description.setText(item.getDescription().substring(0, 32));
        else holder.description.setText(item.getDescription());
        holder.title.setText(item.getName());
        holder.icon.setImageResource(item.getIcon());
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView title, description, point;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.item_icon);
            title = itemView.findViewById(R.id.item_title);
            description = itemView.findViewById(R.id.item_description);
            point = itemView.findViewById(R.id.item_point);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    handler.onItemClick(data.get(getAdapterPosition()), getAdapterPosition(), 0);
                }
            });
        }
    }
}
