package com.aitekteam.developer.tabungyuk.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aitekteam.developer.tabungyuk.R;
import com.aitekteam.developer.tabungyuk.interfaces.CustomOnClickListener;
import com.aitekteam.developer.tabungyuk.models.entities.Wish;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

public class SavingAdapter extends RecyclerView.Adapter<SavingAdapter.ViewHolder> {
    private List<Wish> data;
    private CustomOnClickListener handler;
    public static int VIEW_HISTORY = 0, DO_DEPOSITE = 1;

    public SavingAdapter(CustomOnClickListener handler) {
        this.data = new ArrayList<>();
        this.handler = handler;
    }

    public void setData(List<Wish> data) {
        this.data = data;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_wish, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Wish item = this.data.get(position);
        holder.title.setText(item.getTitle());
        holder.progressText.setText(new StringBuilder()
                .append(setCurrency(item.getPrice_current()))
                .append(" / ")
                .append(setCurrency(item.getPrice_target())));
        holder.labelPoint.setText(String.valueOf(item.getPoint()));
        holder.progressBar.setMax(100);
        holder.progressBar.setProgress((int) ((item.getPrice_current() * 100) / item.getPrice_target()));
        holder.timeRemaining.setText(new StringBuilder()
                .append((item.getCreated_at() - System.currentTimeMillis()) / (1000  * 60 * 60 * 24))
        .append(" days left"));
    }

    private String setCurrency(long harga) {
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("IDR ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);
        return kursIndonesia.format(harga);
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView timeRemaining, title, progressText, labelPoint;
        ProgressBar progressBar;
        Button doDeposit;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            timeRemaining = itemView.findViewById(R.id.item_time_remaining);
            title = itemView.findViewById(R.id.item_title);
            progressText = itemView.findViewById(R.id.item_progress_text);
            labelPoint = itemView.findViewById(R.id.item_label_point);
            progressBar = itemView.findViewById(R.id.item_progress_bar);
            doDeposit = itemView.findViewById(R.id.item_do_deposit);

            title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    handler.onItemClick(data.get(getAdapterPosition()), getAdapterPosition(), VIEW_HISTORY);
                }
            });
            doDeposit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    handler.onItemClick(data.get(getAdapterPosition()), getAdapterPosition(), DO_DEPOSITE);
                }
            });
        }
    }
}
