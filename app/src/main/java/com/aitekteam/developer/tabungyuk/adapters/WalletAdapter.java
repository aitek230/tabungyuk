package com.aitekteam.developer.tabungyuk.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aitekteam.developer.tabungyuk.R;
import com.aitekteam.developer.tabungyuk.models.entities.Expense;
import com.aitekteam.developer.tabungyuk.models.entities.Inclusion;

import java.sql.Date;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.ViewHolder> {
    private int type_call;
    private List<Object> data;
    public static int TYPE_INCLUSION = 0, TYPE_EXPENSE = 1;

    public WalletAdapter(int type_call) {
        this.type_call = type_call;
        this.data = new ArrayList<>();
    }

    public void setData(List<Object> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(this.type_call == TYPE_INCLUSION ? R.layout.item_inclusion : R.layout.item_expense,
                        parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (type_call == TYPE_INCLUSION) {
            bindInclusion(holder, position);
        }
        else {
            bindExpense(holder, position);
        }
    }

    private void bindInclusion(ViewHolder holder, int position) {
        DecimalFormat idrFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        formatRp.setCurrencySymbol("IDR ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        idrFormat.setDecimalFormatSymbols(formatRp);

        Inclusion item = (Inclusion) this.data.get(position);
        holder.labelPoint.setText(String.valueOf(item.getPoint()));
        holder.title.setText(new StringBuilder().append(idrFormat.format(item.getPrice_in())));
        holder.timeRemaining.setText(convertTime(item.getCreated_at()));
    }

    private void bindExpense(ViewHolder holder, int position) {
        DecimalFormat idrFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        formatRp.setCurrencySymbol("IDR ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        idrFormat.setDecimalFormatSymbols(formatRp);

        Expense item = (Expense) this.data.get(position);
        holder.labelPoint.setText(String.valueOf(item.getPoint()));
        holder.title.setText(new StringBuilder().append(idrFormat.format(item.getPrice_out())));
        holder.timeRemaining.setText(convertTime(item.getCreated_at()));
    }

    private String convertTime(long time){
        Date date = new Date(time);
        Format format = new SimpleDateFormat("yyyy MMM dd HH:mm", Locale.ENGLISH);
        return format.format(date);
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    public Object getItemLast() {
        if (this.data.size() == 0) return null;
        return this.data.get(this.data.size() - 1);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView timeRemaining, title, labelPoint;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            timeRemaining = itemView.findViewById(R.id.item_time_remaining);
            title = itemView.findViewById(R.id.item_title);
            labelPoint = itemView.findViewById(R.id.item_label_point);
        }
    }
}