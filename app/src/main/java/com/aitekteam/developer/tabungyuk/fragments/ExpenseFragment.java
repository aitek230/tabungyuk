package com.aitekteam.developer.tabungyuk.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aitekteam.developer.tabungyuk.R;
import com.aitekteam.developer.tabungyuk.adapters.WalletAdapter;
import com.aitekteam.developer.tabungyuk.helpers.DatabaseViewModel;
import com.aitekteam.developer.tabungyuk.helpers.MasterDialog;
import com.aitekteam.developer.tabungyuk.models.entities.Expense;
import com.aitekteam.developer.tabungyuk.models.entities.Users;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

public class ExpenseFragment extends Fragment implements MasterDialog.MasterDialogListener {

    // Declare Variable
    private TextView balance;
    private DatabaseViewModel db;
    private Users profile;
    private RelativeLayout emptyView;
    private WalletAdapter adapter;
    private Observer<List<Expense>> liveExpense = new Observer<List<Expense>>() {
        @Override
        public void onChanged(List<Expense> expenses) {
            emptyView.setVisibility(View.GONE);
            if (expenses.size() > 0)
                adapter.setData(new ArrayList<Object>(expenses));
            else emptyView.setVisibility(View.VISIBLE);
        }
    };

    /**
     * OnCreateView Set View For Fragment
     * @param inflater = Layout Inflater
     * @param container = ViewGroup
     * @param savedInstanceState = Bundle
     * @return View
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    /**
     * OnViewCreated Control The View After Created
     * @param view = View
     * @param savedInstanceState = Bundle
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*
          Initialization
            - Action Button,
            - Bg Mark,
            - Label Balance,
            - Label Balance In,
            - Label Balance Out,
            - Balance In,
            - Balance Out,
            - Balance
         */
        FloatingActionButton actionButton = view.findViewById(R.id.button_action);
        ImageView bgMark = view.findViewById(R.id.bg_mark);
        TextView label_balance = view.findViewById(R.id.label_balance);
        TextView label_balance_in = view.findViewById(R.id.label_balance_in);
        TextView label_balance_out = view.findViewById(R.id.label_balance_out);
        TextView balance_in = view.findViewById(R.id.balance_in);
        TextView balance_out = view.findViewById(R.id.balance_out);
        this.balance = view.findViewById(R.id.balance);
        this.db = new ViewModelProvider(this).get(DatabaseViewModel.class);

        // Initialization Lists
        RecyclerView lists = view.findViewById(R.id.lists);

        // Initialization Empty View
        this.emptyView = view.findViewById(R.id.empty_view);

        // Set Icon From Action Button
        actionButton.setImageResource(R.drawable.ic_account_balance_wallet_black_24dp);

        // Set Bg From BgMark
        bgMark.setImageResource(R.drawable.bg_dark_red);

        // Initialization Adapter
        this.adapter = new WalletAdapter(WalletAdapter.TYPE_EXPENSE);

        // Set Linear Layout Manager To Recycler view
        lists.setLayoutManager(new LinearLayoutManager(getContext()));

        // Set Adapter To Recycler View
        lists.setAdapter(adapter);

        /*
            Hide
            - Label Balance In
            - Label Balance Out
            - Balance In
            - Balance Out
         */
        label_balance_in.setVisibility(View.GONE);
        label_balance_out.setVisibility(View.GONE);
        balance_in.setVisibility(View.GONE);
        balance_out.setVisibility(View.GONE);

        // Set Up Data Wishes
        LiveData<List<Expense>> data = this.db.getExpenses();
        data.observe(getViewLifecycleOwner(), liveExpense);

        // Set Text Label Balance to Balance Out
        label_balance.setText(new StringBuilder().
                append(getString(R.string.label_balance))
                .append(" ")
                .append(getString(R.string.label_balance_out)));
        // Set Color From Balance Out Text
        this.balance.setTextColor(getResources().getColor(R.color.colorPrimaryRed));

        // Handle Action Button After Clicked
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Initialization Master Dialog
                DialogFragment dialog = new MasterDialog(R.layout.dialog_withdraw_balance,
                        R.string.withdraw_balance_name, ExpenseFragment.this);

                // Show Dialog Create Wish List
                if (getActivity() != null)
                    dialog.show(getActivity().getSupportFragmentManager(), "Withdraw Balance");
            }
        });

        getBalanceAmount();
    }

    private void getBalanceAmount() {
        this.profile = this.db.getProfile();
        if (profile != null) {
            DecimalFormat idrFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance();
            DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
            formatRp.setCurrencySymbol("IDR ");
            formatRp.setMonetaryDecimalSeparator(',');
            formatRp.setGroupingSeparator('.');
            idrFormat.setDecimalFormatSymbols(formatRp);
            balance.setText(new StringBuilder().append(idrFormat.format(profile.getWallet_out())));
        }
    }

    /**
     * On Dialog Positive Click
     * @param dialog = Dialog Fragment
     * @param view = View
     */
    @Override
    public void onDialogPositiveClick(DialogFragment dialog, View view) {
        EditText amountOut = view.findViewById(R.id.amount_in);
        String valAmountOut = amountOut.getText().toString();
        if (TextUtils.isEmpty(valAmountOut)) {
            Toast.makeText(getContext(), R.string.msg_form_need_value, Toast.LENGTH_LONG).show();
        }
        else if (Integer.parseInt(valAmountOut) <= 0) {
            Toast.makeText(getContext(), R.string.msg_form_need_value_more_than_zero, Toast.LENGTH_LONG).show();
        }
        else {
            Expense expense = new Expense();
            expense.setCreated_at(System.currentTimeMillis());
            expense.setUpdated_at(System.currentTimeMillis());
            expense.setExpense_id("");
            expense.setUser_id("");
            expense.setPoint(-5);
            expense.setPrice_out(Long.parseLong(valAmountOut));

            this.db.doAction(expense, DatabaseViewModel.ACTION_INSERT, DatabaseViewModel.TB_EXPENSE);

            if (this.profile != null) {
                this.profile.setWallet_out(this.profile.getWallet_out() + Long.parseLong(valAmountOut));
                this.profile.setWallet(this.profile.getWallet() - Long.parseLong(valAmountOut));
                this.profile.setWallet_in(this.profile.getWallet_in() - Long.parseLong(valAmountOut));
                this.db.doAction(this.profile, DatabaseViewModel.ACTION_EDIT, DatabaseViewModel.TB_USER);
                getBalanceAmount();
            }
        }

        dialog.dismiss();
    }

    /**
     * On Dialog Negative Click
     * @param dialog = Dialog Fragment
     * @param view = View
     */
    @Override
    public void onDialogNegativeClick(DialogFragment dialog, View view) {
        dialog.dismiss();
    }

    /**
     * On Show Dialog
     * @param dialog = Dialog Fragment
     * @param view = View
     */
    @Override
    public void onShow(DialogFragment dialog, View view) {

    }
}
