package com.aitekteam.developer.tabungyuk.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aitekteam.developer.tabungyuk.R;
import com.aitekteam.developer.tabungyuk.adapters.SavingAdapter;
import com.aitekteam.developer.tabungyuk.helpers.CompleteDialog;
import com.aitekteam.developer.tabungyuk.helpers.DatabaseViewModel;
import com.aitekteam.developer.tabungyuk.helpers.DateDialog;
import com.aitekteam.developer.tabungyuk.helpers.MasterDialog;
import com.aitekteam.developer.tabungyuk.helpers.RandomString;
import com.aitekteam.developer.tabungyuk.interfaces.CustomOnClickListener;
import com.aitekteam.developer.tabungyuk.models.data.CompletePoint;
import com.aitekteam.developer.tabungyuk.models.entities.Users;
import com.aitekteam.developer.tabungyuk.models.entities.Wish;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class HomeFragment extends Fragment implements MasterDialog.MasterDialogListener, CompleteDialog.CompleteDialogListener {

    private SavingAdapter adapter;
    private DatabaseViewModel db;
    private RelativeLayout emptyView;
    private TextView balance, balanceIn, balanceOut;
    private Users profile;
    private DecimalFormat idrFormat;
    private DecimalFormatSymbols formatRp;
    private Observer<List<Wish>> liveWish = new Observer<List<Wish>>() {
        @Override
        public void onChanged(List<Wish> wishes) {
            emptyView.setVisibility(View.GONE);
            if (wishes.size() > 0) {
                List<Wish> data = new ArrayList<>();
                for (Wish item: wishes) {
                    if (item.getStatus() != 1) {
                        data.add(item);
                    }
                }
                if (data.size() > 0) adapter.setData(data);
                else emptyView.setVisibility(View.VISIBLE);
            }
            else emptyView.setVisibility(View.VISIBLE);
        }
    };
    private Observer<Users> liveProfile = new Observer<Users>() {
        @Override
        public void onChanged(Users users) {
            if (users != null) {
                profile = users;
                balance.setText(new StringBuilder().append(idrFormat.format(users.getWallet())));
                balanceIn.setText(new StringBuilder().append(idrFormat.format(users.getWallet_in())));
                balanceOut.setText(new StringBuilder().append(idrFormat.format(users.getWallet_out())));
            }
        }
    };

    /**
     * OnCreateView Set View For Fragment
     * @param inflater = Layout Inflater
     * @param container = ViewGroup
     * @param savedInstanceState = Bundle
     * @return View
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    /**
     * OnViewCreated Control The View After Created
     * @param view = View
     * @param savedInstanceState = Bundle
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialization Database View Model
        this.db = new ViewModelProvider(this).get(DatabaseViewModel.class);

        // Initialization Action Button
        FloatingActionButton actionButton = view.findViewById(R.id.button_action);

        // Initialization Lists
        RecyclerView lists = view.findViewById(R.id.lists);

        // Initialization Empty View
        this.emptyView = view.findViewById(R.id.empty_view);

        this.balance = view.findViewById(R.id.balance);
        this.balanceIn = view.findViewById(R.id.balance_in);
        this.balanceOut = view.findViewById(R.id.balance_out);

        idrFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("IDR ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        idrFormat.setDecimalFormatSymbols(formatRp);

        // Initialization Adapter
        this.adapter = new SavingAdapter(new CustomOnClickListener() {
            @Override
            public void onItemClick(Object item, int position, int action) {
                Wish currentItem = (Wish) item;
                if (action == SavingAdapter.VIEW_HISTORY) {
                    // TODO:: Go To View History Deposit
                }
                else {
                    // TODO:: Do Deposit
                    if (profile != null) {
                        if (profile.getWallet() < currentItem.getDefault_deposit()) {
                            Toast.makeText(getContext(), R.string.msg_balance_is_not_sufficient, Toast.LENGTH_LONG).show();
                            return;
                        }

                        if (currentItem.getPoint() != 1 && currentItem.getUpdated_at() != 0) {
                            if (convertTime(currentItem.getUpdated_at()).equalsIgnoreCase(convertTime(System.currentTimeMillis()))) {
                                Toast.makeText(getContext(), R.string.msg_you_saved_up_today, Toast.LENGTH_LONG).show();
                                return;
                            }
                        }

                        currentItem.setPrice_current(
                                (currentItem.getPrice_current() + currentItem.getDefault_deposit())
                        );
                        currentItem.setUpdated_at(System.currentTimeMillis());


                        if (currentItem.getPrice_current() >= currentItem.getPrice_target()) {
                            currentItem.setStatus(1);
                            profile.setPoint((profile.getPoint() + currentItem.getPoint()));

                            // Initialization Complete Dialog
                            DialogFragment dialog = new CompleteDialog(HomeFragment.this,
                                    R.layout.dialog_complete, R.string.quest_complete_name, new CompletePoint(
                                            "Berhasil menyelesaikan tabungan " + currentItem.getTitle(),
                                    currentItem.getPoint()
                            ));

                            // Show Dialog
                            if (getActivity() != null)
                                dialog.show(getActivity().getSupportFragmentManager(), "Complete Saving");
                        }

                        db.doAction(currentItem, DatabaseViewModel.ACTION_EDIT, DatabaseViewModel.TB_WISH);
                        adapter.notifyItemChanged(position);

                        profile.setWallet((profile.getWallet() - currentItem.getDefault_deposit()));
                        profile.setWallet_in((profile.getWallet_in() - currentItem.getDefault_deposit()));
                        db.doAction(profile, DatabaseViewModel.ACTION_EDIT, DatabaseViewModel.TB_USER);

                        Toast.makeText(getContext(), new StringBuilder()
                                .append(getString(R.string.msg_success_saved_balance))
                                .append(idrFormat.format(currentItem.getDefault_deposit()))
                                .append(" ke ")
                                .append(currentItem.getTitle()), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        // Set Linear Layout Manager To Recycler view
        lists.setLayoutManager(new LinearLayoutManager(getContext()));

        // Set Adapter To Recycler View
        lists.setAdapter(adapter);

        // Check If Already Have Account
        if (!this.db.checkAlreadyInstaller()) {
            Users newUser = new Users();
            newUser.setUser_id("");
            newUser.setUsername(RandomString.getAlphaNumericString(20));
            newUser.setPassword("");
            newUser.setFull_name("User " + RandomString.getAlphaNumericString(10));
            newUser.setFace("");
            newUser.setWallet(0);
            newUser.setWallet_in(0);
            newUser.setWallet_out(0);
            newUser.setPoint(0);
            newUser.setCreated_at(System.currentTimeMillis());
            newUser.setUpdated_at(System.currentTimeMillis());
            this.db.doAction(newUser, DatabaseViewModel.ACTION_INSERT, DatabaseViewModel.TB_USER);
        }

        // Set Up Data Wishes
        LiveData<List<Wish>> data = this.db.getWishs();
        data.observe(getViewLifecycleOwner(), liveWish);

        // Set Up Live Profile
        LiveData<Users> liveDataProfile = this.db.liveProfile();
        liveDataProfile.observe(getViewLifecycleOwner(), liveProfile);

        // Handle Action Button After Clicked
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Initialization Master Dialog
                DialogFragment dialog = new MasterDialog(R.layout.dialog_create_saving,
                        R.string.create_wish_name, HomeFragment.this);

                // Show Dialog Create Wish List
                if (getActivity() != null)
                dialog.show(getActivity().getSupportFragmentManager(), "Create Saving");
            }
        });

        getBalanceAmount();
    }

    private String convertTime(long time){
        java.sql.Date date = new java.sql.Date(time);
        Format format = new SimpleDateFormat("yyyy MMM dd", Locale.ENGLISH);
        return format.format(date);
    }

    private void getBalanceAmount() {
        this.profile = this.db.getProfile();
        if (profile != null) {
            balance.setText(new StringBuilder().append(idrFormat.format(profile.getWallet())));
            balanceIn.setText(new StringBuilder().append(idrFormat.format(profile.getWallet_in())));
            balanceOut.setText(new StringBuilder().append(idrFormat.format(profile.getWallet_out())));
        }
    }

    /**
     * On Dialog Positive Click
     * @param dialog = Dialog Fragment
     * @param view = View
     */
    @Override
    public void onDialogPositiveClick(DialogFragment dialog, View view) {
        EditText wishName = view.findViewById(R.id.wish_name);
        EditText wishPrice = view.findViewById(R.id.wish_price);
        TextView wishDate = view.findViewById(R.id.wish_date);

        try {
            Date date = new SimpleDateFormat("dd/MM/yyyy", Locale.US)
                    .parse(wishDate.getText().toString());
            Timestamp timestamp = new Timestamp(date != null ? date.getTime() : System.currentTimeMillis());
            Wish wish = new Wish();
            wish.setCreated_at(timestamp.getTime());
            wish.setPrice_current(0);
            wish.setPrice_target(Long.parseLong(wishPrice.getText().toString()));
            wish.setStatus(0);
            wish.setTitle(wishName.getText().toString());
            wish.setUpdated_at(0);
            wish.setUser_id("");
            wish.setWish_id("");
            if (((timestamp.getTime() - System.currentTimeMillis()) / (1000  * 60 * 60 * 24)) < 1) {
                wish.setPoint(1);
                wish.setDefault_deposit(Long.parseLong(wishPrice.getText().toString()));
            }
            else {
                wish.setPoint((timestamp.getTime() - System.currentTimeMillis()) / (1000  * 60 * 60 * 24));
                wish.setDefault_deposit(Long.parseLong(wishPrice.getText().toString()) /
                        ((timestamp.getTime() - System.currentTimeMillis()) / (1000  * 60 * 60 * 24)));
            }

            db.doAction(wish, DatabaseViewModel.ACTION_INSERT, DatabaseViewModel.TB_WISH);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        dialog.dismiss();
    }

    /**
     * On Dialog Negative Click
     * @param dialog = Dialog Fragment
     * @param view = View
     */
    @Override
    public void onDialogNegativeClick(DialogFragment dialog, View view) {
            dialog.dismiss();
    }

    /**
     * On Show Dialog
     * @param dialog = Dialog Fragment
     * @param view = View
     */
    @Override
    public void onShow(DialogFragment dialog, View view) {
        // Initialize Wish Date
        final TextView wishDate = view.findViewById(R.id.wish_date);

        // Handle Action If Wish Date is Clicked
        wishDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Initialize And Create handle For Date Picker
                DialogFragment dateFragment = new DateDialog(new DateDialog.DateDialogListener() {
                    @Override
                    public void getDate(int year, int month, int day) {
                        // Set Text Date To Wish Date
                        wishDate.setText(new StringBuilder()
                        .append(day).append("/")
                        .append((month + 1)).append("/")
                        .append(year));
                    }
                });

                // Show Date Picker
                if (getActivity() != null)
                    dateFragment.show(getActivity().getSupportFragmentManager(), "Date Saving");
            }
        });
    }

    @Override
    public void onDestroy() {
        this.liveWish = null;
        super.onDestroy();
    }

    @Override
    public void onCompleteShow(final DialogFragment dialog, View view, CompletePoint data) {
        TextView point, message;
        Button claim;

        point = view.findViewById(R.id.point);
        message = view.findViewById(R.id.message);
        claim = view.findViewById(R.id.claim);

        point.setText(new StringBuilder().append(data.getPoint()).append(" point"));
        message.setText(data.getMessage());
        claim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }
}
