package com.aitekteam.developer.tabungyuk.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aitekteam.developer.tabungyuk.R;
import com.aitekteam.developer.tabungyuk.adapters.WalletAdapter;
import com.aitekteam.developer.tabungyuk.helpers.CompleteDialog;
import com.aitekteam.developer.tabungyuk.helpers.DatabaseViewModel;
import com.aitekteam.developer.tabungyuk.helpers.MasterDialog;
import com.aitekteam.developer.tabungyuk.models.data.CompletePoint;
import com.aitekteam.developer.tabungyuk.models.entities.Inclusion;
import com.aitekteam.developer.tabungyuk.models.entities.Users;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class InclusionFragment extends Fragment implements MasterDialog.MasterDialogListener, CompleteDialog.CompleteDialogListener {

    // Declare Variable
    private TextView balance;
    private DatabaseViewModel db;
    private Users profile;
    private RelativeLayout emptyView;
    private WalletAdapter adapter;
    private Observer<List<Inclusion>> liveInclusion = new Observer<List<Inclusion>>() {
        @Override
        public void onChanged(List<Inclusion> inclusions) {
            emptyView.setVisibility(View.GONE);
            if (inclusions.size() > 0)
                adapter.setData(new ArrayList<Object>(inclusions));
            else emptyView.setVisibility(View.VISIBLE);
        }
    };

    /**
     * OnCreateView Set View For Fragment
     * @param inflater = Layout Inflater
     * @param container = ViewGroup
     * @param savedInstanceState = Bundle
     * @return View
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    /**
     * OnViewCreated Control The View After Created
     * @param view = View
     * @param savedInstanceState = Bundle
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*
          Initialization
            - Action Button,
            - Bg Mark,
            - Label Balance,
            - Label Balance In,
            - Label Balance Out,
            - Balance In,
            - Balance Out,
            - Balance
         */
        FloatingActionButton actionButton = view.findViewById(R.id.button_action);
        ImageView bgMark = view.findViewById(R.id.bg_mark);
        TextView label_balance = view.findViewById(R.id.label_balance);
        TextView label_balance_in = view.findViewById(R.id.label_balance_in);
        TextView label_balance_out = view.findViewById(R.id.label_balance_out);
        TextView balance_in = view.findViewById(R.id.balance_in);
        TextView balance_out = view.findViewById(R.id.balance_out);
        this.balance = view.findViewById(R.id.balance);
        this.db = new ViewModelProvider(this).get(DatabaseViewModel.class);

        // Initialization Lists
        RecyclerView lists = view.findViewById(R.id.lists);

        // Initialization Empty View
        this.emptyView = view.findViewById(R.id.empty_view);

        // Set Icon From Action Button
        actionButton.setImageResource(R.drawable.ic_account_balance_wallet_black_24dp);

        // Set Bg From BgMark
        bgMark.setImageResource(R.drawable.bg_dark_blue);

        // Initialization Adapter
        this.adapter = new WalletAdapter(WalletAdapter.TYPE_INCLUSION);

        // Set Linear Layout Manager To Recycler view
        lists.setLayoutManager(new LinearLayoutManager(getContext()));

        // Set Adapter To Recycler View
        lists.setAdapter(adapter);

        /*
            Hide
            - Label Balance In
            - Label Balance Out
            - Balance In
            - Balance Out
         */
        label_balance_in.setVisibility(View.GONE);
        label_balance_out.setVisibility(View.GONE);
        balance_in.setVisibility(View.GONE);
        balance_out.setVisibility(View.GONE);

        // Set Up Data Wishes
        LiveData<List<Inclusion>> data = this.db.getInclusions();
        data.observe(getViewLifecycleOwner(), liveInclusion);

        // Set Text Label Balance to Balance In
        label_balance.setText(new StringBuilder().
                append(getString(R.string.label_balance))
                .append(" ")
                .append(getString(R.string.label_balance_in)));
        // Set Color From Balance In Text
        this.balance.setTextColor(getResources().getColor(R.color.colorPrimaryBlue));

        // Handle Action Button After Clicked
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Initialization Master Dialog
                DialogFragment dialog = new MasterDialog(R.layout.dialog_insert_balance,
                        R.string.insert_balance_name, InclusionFragment.this);

                // Show Dialog Create Wish List
                if (getActivity() != null)
                    dialog.show(getActivity().getSupportFragmentManager(), "Insert Balance");
            }
        });

        getBalanceAmount();
    }

    private void getBalanceAmount() {
        this.profile = this.db.getProfile();
        if (profile != null) {
            DecimalFormat idrFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance();
            DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
            formatRp.setCurrencySymbol("IDR ");
            formatRp.setMonetaryDecimalSeparator(',');
            formatRp.setGroupingSeparator('.');
            idrFormat.setDecimalFormatSymbols(formatRp);
            balance.setText(new StringBuilder().append(idrFormat.format(profile.getWallet_in())));
        }
    }

    /**
     * On Dialog Positive Click
     * @param dialog = Dialog Fragment
     * @param view = View
     */
    @Override
    public void onDialogPositiveClick(DialogFragment dialog, View view) {
        EditText amountIn = view.findViewById(R.id.amount_in);
        String valAmountIn = amountIn.getText().toString();
        if (TextUtils.isEmpty(valAmountIn)) {
            Toast.makeText(getContext(), R.string.msg_form_need_value, Toast.LENGTH_LONG).show();
        }
        else if (Integer.parseInt(valAmountIn) <= 0) {
            Toast.makeText(getContext(), R.string.msg_form_need_value_more_than_zero, Toast.LENGTH_LONG).show();
        }
        else {

            if (adapter.getItemLast() != null) {
                Inclusion item = (Inclusion) adapter.getItemLast();
                if (convertTime(item.getCreated_at())
                        .equalsIgnoreCase(convertTime(System.currentTimeMillis()))) {
                    Toast.makeText(getContext(), R.string.msg_you_have_topped_up_your_balance_today, Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                    return;
                }
            }

            Inclusion inclusion = new Inclusion();
            inclusion.setCreated_at(System.currentTimeMillis());
            inclusion.setUpdated_at(System.currentTimeMillis());
            inclusion.setInclusion_id("");
            inclusion.setUser_id("");
            inclusion.setPoint(5);
            inclusion.setPrice_in(Long.parseLong(valAmountIn));

            this.db.doAction(inclusion, DatabaseViewModel.ACTION_INSERT, DatabaseViewModel.TB_INCLUSION);

            // Initialization Complete Dialog
            DialogFragment completeDialog = new CompleteDialog(InclusionFragment.this,
                    R.layout.dialog_complete, R.string.quest_complete_name, new CompletePoint(
                    "Berhasil mengisi saldo",
                    inclusion.getPoint()
            ));

            // Show Dialog
            if (getActivity() != null)
                completeDialog.show(getActivity().getSupportFragmentManager(), "Complete Saving");

            if (this.profile != null) {
                this.profile.setWallet_in(this.profile.getWallet_in() + Long.parseLong(valAmountIn));
                this.profile.setWallet(this.profile.getWallet() + Long.parseLong(valAmountIn));
                this.profile.setPoint(this.profile.getPoint() + 5);
                this.db.doAction(this.profile, DatabaseViewModel.ACTION_EDIT, DatabaseViewModel.TB_USER);
                getBalanceAmount();
            }
        }

        dialog.dismiss();
    }

    private String convertTime(long time){
        java.sql.Date date = new java.sql.Date(time);
        Format format = new SimpleDateFormat("yyyy MMM dd", Locale.ENGLISH);
        return format.format(date);
    }

    /**
     * On Dialog Negative Click
     * @param dialog = Dialog Fragment
     * @param view = View
     */
    @Override
    public void onDialogNegativeClick(DialogFragment dialog, View view) {
        dialog.dismiss();
    }

    /**
     * On Show Dialog
     * @param dialog = Dialog Fragment
     * @param view = View
     */
    @Override
    public void onShow(DialogFragment dialog, View view) {

    }

    @Override
    public void onCompleteShow(final DialogFragment dialog, View view, CompletePoint data) {
        TextView point, message;
        Button claim;

        point = view.findViewById(R.id.point);
        message = view.findViewById(R.id.message);
        claim = view.findViewById(R.id.claim);

        point.setText(new StringBuilder().append(data.getPoint()).append(" point"));
        message.setText(data.getMessage());
        claim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }
}