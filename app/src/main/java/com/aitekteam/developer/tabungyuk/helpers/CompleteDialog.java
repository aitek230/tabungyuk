package com.aitekteam.developer.tabungyuk.helpers;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.aitekteam.developer.tabungyuk.models.data.CompletePoint;

public class CompleteDialog extends DialogFragment {

    public interface CompleteDialogListener {
        public void onCompleteShow(DialogFragment dialog, View view, CompletePoint data);
    }

    private CompleteDialogListener listener;
    private int layout, title;
    private CompletePoint data;

    public CompleteDialog(CompleteDialogListener listener, int layout, int title, CompletePoint data) {
        this.listener = listener;
        this.layout = layout;
        this.title = title;
        this.data = data;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        final View view = inflater.inflate(this.layout, null);
        builder.setView(view)
                .setTitle(this.title);
        final AlertDialog dialog = builder.create();
        listener.onCompleteShow(CompleteDialog.this, view, this.data);
        return dialog;
    }
}
