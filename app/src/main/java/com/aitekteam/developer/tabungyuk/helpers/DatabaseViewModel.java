package com.aitekteam.developer.tabungyuk.helpers;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.aitekteam.developer.tabungyuk.models.AppDatabases;
import com.aitekteam.developer.tabungyuk.models.entities.Expense;
import com.aitekteam.developer.tabungyuk.models.entities.Garden;
import com.aitekteam.developer.tabungyuk.models.entities.Inclusion;
import com.aitekteam.developer.tabungyuk.models.entities.Users;
import com.aitekteam.developer.tabungyuk.models.entities.Wish;
import com.aitekteam.developer.tabungyuk.models.queries.ExpenseQuery;
import com.aitekteam.developer.tabungyuk.models.queries.GardenQuery;
import com.aitekteam.developer.tabungyuk.models.queries.InclusionQuery;
import com.aitekteam.developer.tabungyuk.models.queries.UsersQuery;
import com.aitekteam.developer.tabungyuk.models.queries.WishQuery;

import java.util.List;

public class DatabaseViewModel extends AndroidViewModel {

    public static int ACTION_INSERT = 0;
    public static int ACTION_DELETE = 1;
    public static int ACTION_EDIT = 2;

    public static String TB_USER = "users";
    public static String TB_WISH = "wish";
    public static String TB_INCLUSION = "inclusion";
    public static String TB_EXPENSE = "expense";
    public static String TB_GARDEN = "garden";

    private UsersQuery usersQuery;
    private WishQuery wishQuery;
    private InclusionQuery inclusionQuery;
    private ExpenseQuery expenseQuery;
    private GardenQuery gardenQuery;

    public DatabaseViewModel(@NonNull Application application) {
        super(application);

        AppDatabases db = AppDatabases.getInstance(application);
        this.usersQuery = db.usersQuery();
        this.wishQuery = db.wishQuery();
        this.inclusionQuery = db.inclusionQuery();
        this.expenseQuery = db.expenseQuery();
        this.gardenQuery = db.gardenQuery();
    }

    public Users getProfile() {
        return this.usersQuery.loadProfile();
    }

    public LiveData<Users> liveProfile() {
        return this.usersQuery.liveProfile();
    }

    public boolean checkAlreadyInstaller() {
        return this.usersQuery.checkProfile() > 0;
    }

    public LiveData<List<Wish>> getWishs() {
        return this.wishQuery.getAllWishs();
    }

    public LiveData<List<Inclusion>> getInclusions() {
        return this.inclusionQuery.getAllInclusions();
    }

    public LiveData<List<Expense>> getExpenses() {
        return this.expenseQuery.getAllExpenses();
    }

    public LiveData<List<Garden>> getGardens() {
        return this.gardenQuery.getAllGarden();
    }

    public LiveData<Wish> getWish(int id) {
        return this.wishQuery.getWish(id);
    }

    public LiveData<Inclusion> getInclusion(int id) {
        return this.inclusionQuery.getInclusion(id);
    }

    public LiveData<Expense> getExpense(int id) {
        return this.expenseQuery.getExpense(id);
    }

    public void doAction(Object data, int action, String table) {
        if (table.equalsIgnoreCase(TB_USER)) {
            if (action == ACTION_INSERT) usersQuery.insert((Users) data);
            else if (action == ACTION_EDIT) usersQuery.update((Users) data);
            else if (action == ACTION_DELETE) usersQuery.delete((Users) data);
        }
        else if (table.equalsIgnoreCase(TB_WISH)) {
            if (action == ACTION_INSERT) wishQuery.insert((Wish) data);
            else if (action == ACTION_EDIT) wishQuery.update((Wish) data);
            else if (action == ACTION_DELETE) wishQuery.delete((Wish) data);
        }
        else if (table.equalsIgnoreCase(TB_INCLUSION)) {
            if (action == ACTION_INSERT) inclusionQuery.insert((Inclusion) data);
            else if (action == ACTION_EDIT) inclusionQuery.update((Inclusion) data);
            else if (action == ACTION_DELETE) inclusionQuery.delete((Inclusion) data);
        }
        else if (table.equalsIgnoreCase(TB_EXPENSE)) {
            if (action == ACTION_INSERT) expenseQuery.insert((Expense) data);
            else if (action == ACTION_EDIT) expenseQuery.update((Expense) data);
            else if (action == ACTION_DELETE) expenseQuery.delete((Expense) data);
        }
        else if(table.equalsIgnoreCase(TB_GARDEN)) {
            if (action == ACTION_INSERT) gardenQuery.insert((Garden) data);
            else if (action == ACTION_EDIT) gardenQuery.update((Garden) data);
            else if (action == ACTION_DELETE) gardenQuery.delete((Garden) data);
        }
        else {
            Log.d("TB_NOT_FOUND", "DATABASE NOT FOUND");
        }
    }

}
