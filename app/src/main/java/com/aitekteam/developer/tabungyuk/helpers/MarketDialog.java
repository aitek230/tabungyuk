package com.aitekteam.developer.tabungyuk.helpers;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.aitekteam.developer.tabungyuk.models.data.ItemMarket;

public class MarketDialog extends DialogFragment {
    public interface MarketDialogListener {
        public void onShow(DialogFragment dialog, View view, ItemMarket data);
    }

    private MarketDialogListener listener;
    private int layout;
    private String title;
    private ItemMarket data;

    public MarketDialog(MarketDialogListener listener, int layout, String title, ItemMarket data) {
        this.listener = listener;
        this.layout = layout;
        this.title = title;
        this.data = data;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        final View view = inflater.inflate(this.layout, null);
        builder.setView(view)
                .setTitle(this.title);
        final AlertDialog dialog = builder.create();
        listener.onShow(MarketDialog.this, view, this.data);
        return dialog;
    }
}
