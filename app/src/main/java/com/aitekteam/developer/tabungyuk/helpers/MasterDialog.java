package com.aitekteam.developer.tabungyuk.helpers;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.aitekteam.developer.tabungyuk.R;

public class MasterDialog extends DialogFragment {

    public interface MasterDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog, View view);
        public void onDialogNegativeClick(DialogFragment dialog, View view);
        public void onShow(DialogFragment dialog, View view);
    }

    private MasterDialogListener listener;
    private int layout, title;

    public MasterDialog(int layout, int title, MasterDialogListener listener) {
        this.layout = layout;
        this.title = title;
        this.listener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        final View view = inflater.inflate(this.layout, null);
        builder.setView(view)
                .setTitle(this.title)
                .setPositiveButton(R.string.button_save, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onDialogPositiveClick(MasterDialog.this, view);
            }
        }).setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onDialogNegativeClick(MasterDialog.this, view);
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryRed));
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryBlue));
            }
        });
        listener.onShow(MasterDialog.this, view);
        return dialog;
    }
}
