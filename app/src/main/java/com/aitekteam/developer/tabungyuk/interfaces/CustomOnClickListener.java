package com.aitekteam.developer.tabungyuk.interfaces;

public interface CustomOnClickListener {
    void onItemClick(Object item, int position, int action);
}
