package com.aitekteam.developer.tabungyuk.models;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.aitekteam.developer.tabungyuk.models.entities.Expense;
import com.aitekteam.developer.tabungyuk.models.entities.Garden;
import com.aitekteam.developer.tabungyuk.models.entities.Inclusion;
import com.aitekteam.developer.tabungyuk.models.entities.Users;
import com.aitekteam.developer.tabungyuk.models.entities.Wish;
import com.aitekteam.developer.tabungyuk.models.queries.ExpenseQuery;
import com.aitekteam.developer.tabungyuk.models.queries.GardenQuery;
import com.aitekteam.developer.tabungyuk.models.queries.InclusionQuery;
import com.aitekteam.developer.tabungyuk.models.queries.UsersQuery;
import com.aitekteam.developer.tabungyuk.models.queries.WishQuery;

@Database(entities = {Users.class, Wish.class, Inclusion.class, Expense.class, Garden.class}, version = 1, exportSchema = false)
public abstract class AppDatabases extends RoomDatabase {
    private static AppDatabases INSTANCE;

    public abstract UsersQuery usersQuery();
    public abstract WishQuery wishQuery();
    public abstract InclusionQuery inclusionQuery();
    public abstract ExpenseQuery expenseQuery();
    public abstract GardenQuery gardenQuery();

    public static synchronized AppDatabases getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabases.class, "DB_TabungYuk").allowMainThreadQueries().build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
