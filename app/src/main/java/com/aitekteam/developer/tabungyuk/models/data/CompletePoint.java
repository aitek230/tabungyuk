package com.aitekteam.developer.tabungyuk.models.data;

import java.io.Serializable;

public class CompletePoint implements Serializable {
    private String message;
    private long point;

    public CompletePoint(String message, long point) {
        this.message = message;
        this.point = point;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getPoint() {
        return point;
    }

    public void setPoint(long point) {
        this.point = point;
    }
}
