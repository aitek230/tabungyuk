package com.aitekteam.developer.tabungyuk.models.data;

import java.io.Serializable;

public class ItemMarket implements Serializable {
    private String name;
    private String description;
    private int icon;
    private long price;

    public ItemMarket(String name, String description, int icon, long price) {
        this.name = name;
        this.description = description;
        this.icon = icon;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }
}
