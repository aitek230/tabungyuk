package com.aitekteam.developer.tabungyuk.models.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "expense_entity")
public class Expense {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String user_id;
    private String expense_id;
    private long price_out;
    private long point;
    private long created_at;
    private long updated_at;

    public Expense() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getExpense_id() {
        return expense_id;
    }

    public void setExpense_id(String expense_id) {
        this.expense_id = expense_id;
    }

    public long getPrice_out() {
        return price_out;
    }

    public void setPrice_out(long price_out) {
        this.price_out = price_out;
    }

    public long getPoint() {
        return point;
    }

    public void setPoint(long point) {
        this.point = point;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }
}
