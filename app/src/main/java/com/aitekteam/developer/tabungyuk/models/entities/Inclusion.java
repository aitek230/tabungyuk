package com.aitekteam.developer.tabungyuk.models.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "inclusion_entity")
public class Inclusion {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String user_id;
    private String inclusion_id;
    private long price_in;
    private long point;
    private long created_at;
    private long updated_at;

    public Inclusion() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getInclusion_id() {
        return inclusion_id;
    }

    public void setInclusion_id(String inclusion_id) {
        this.inclusion_id = inclusion_id;
    }

    public long getPrice_in() {
        return price_in;
    }

    public void setPrice_in(long price_in) {
        this.price_in = price_in;
    }

    public long getPoint() {
        return point;
    }

    public void setPoint(long point) {
        this.point = point;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }
}
