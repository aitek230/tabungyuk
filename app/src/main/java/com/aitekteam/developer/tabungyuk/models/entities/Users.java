package com.aitekteam.developer.tabungyuk.models.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "users_entity")
public class Users {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String user_id;
    private String username;
    private String password;
    private String full_name;
    private String face;
    private long wallet;
    private long wallet_in;
    private long wallet_out;
    private long point;
    private long created_at;
    private long updated_at;

    public Users() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public long getPoint() {
        return point;
    }

    public void setPoint(long point) {
        this.point = point;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public long getWallet() {
        return wallet;
    }

    public void setWallet(long wallet) {
        this.wallet = wallet;
    }

    public long getWallet_in() {
        return wallet_in;
    }

    public void setWallet_in(long wallet_in) {
        this.wallet_in = wallet_in;
    }

    public long getWallet_out() {
        return wallet_out;
    }

    public void setWallet_out(long wallet_out) {
        this.wallet_out = wallet_out;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }
}
