package com.aitekteam.developer.tabungyuk.models.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "wish_entity")
public class Wish {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private int status;
    private String user_id;
    private String wish_id;
    private String title;
    private long price_target;
    private long price_current;
    private long default_deposit;
    private long point;
    private long created_at;
    private long updated_at;

    public Wish() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getDefault_deposit() {
        return default_deposit;
    }

    public void setDefault_deposit(long default_deposit) {
        this.default_deposit = default_deposit;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getWish_id() {
        return wish_id;
    }

    public void setWish_id(String wish_id) {
        this.wish_id = wish_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getPrice_target() {
        return price_target;
    }

    public void setPrice_target(long price_target) {
        this.price_target = price_target;
    }

    public long getPrice_current() {
        return price_current;
    }

    public void setPrice_current(long price_current) {
        this.price_current = price_current;
    }

    public long getPoint() {
        return point;
    }

    public void setPoint(long point) {
        this.point = point;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }
}
