package com.aitekteam.developer.tabungyuk.models.queries;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.aitekteam.developer.tabungyuk.models.entities.Expense;

import java.util.List;

@Dao
public interface ExpenseQuery {

    @Insert
    void insert(Expense note);

    @Update
    void update(Expense note);

    @Delete
    void delete(Expense note);

    @Query("SELECT * FROM expense_entity ORDER BY id DESC")
    LiveData<List<Expense>> getAllExpenses();

    @Query("SELECT * FROM expense_entity WHERE id = :id")
    LiveData<Expense> getExpense(int id);

}
