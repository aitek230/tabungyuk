package com.aitekteam.developer.tabungyuk.models.queries;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.aitekteam.developer.tabungyuk.models.entities.Garden;

import java.util.List;

@Dao
public interface GardenQuery {
    @Insert
    void insert(Garden note);

    @Update
    void update(Garden note);

    @Delete
    void delete(Garden note);

    @Query("SELECT * FROM flower_entity ORDER BY id DESC")
    LiveData<List<Garden>> getAllGarden();

    @Query("SELECT * FROM flower_entity WHERE id = :id")
    LiveData<Garden> getGarden(int id);
}
