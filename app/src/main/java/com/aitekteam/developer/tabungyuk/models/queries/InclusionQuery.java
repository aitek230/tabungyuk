package com.aitekteam.developer.tabungyuk.models.queries;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.aitekteam.developer.tabungyuk.models.entities.Inclusion;

import java.util.List;

@Dao
public interface InclusionQuery {
    @Insert
    void insert(Inclusion note);

    @Update
    void update(Inclusion note);

    @Delete
    void delete(Inclusion note);

    @Query("SELECT * FROM inclusion_entity ORDER BY id DESC")
    LiveData<List<Inclusion>> getAllInclusions();

    @Query("SELECT * FROM inclusion_entity WHERE id = :id")
    LiveData<Inclusion> getInclusion(int id);
}
