package com.aitekteam.developer.tabungyuk.models.queries;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.aitekteam.developer.tabungyuk.models.entities.Users;

@Dao
public interface UsersQuery {

    @Insert
    void insert(Users user);

    @Update
    void update(Users user);

    @Delete
    void delete(Users user);

    @Query("SELECT * FROM users_entity ORDER BY id DESC LIMIT 1")
    Users loadProfile();

    @Query("SELECT * FROM users_entity ORDER BY id DESC LIMIT 1")
    LiveData<Users> liveProfile();

    @Query("SELECT COUNT(id) FROM users_entity")
    int checkProfile();

}
