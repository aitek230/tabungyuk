package com.aitekteam.developer.tabungyuk.models.queries;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.aitekteam.developer.tabungyuk.models.entities.Wish;

import java.util.List;

@Dao
public interface WishQuery {

    @Insert
    void insert(Wish note);

    @Update
    void update(Wish note);

    @Delete
    void delete(Wish note);

    @Query("SELECT * FROM wish_entity ORDER BY id DESC")
    LiveData<List<Wish>> getAllWishs();

    @Query("SELECT * FROM wish_entity WHERE id = :id")
    LiveData<Wish> getWish(int id);

}
